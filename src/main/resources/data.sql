INSERT INTO faculty (id, name, code, no_of_department, phone_number, email)
VALUES (101, 'Faculty of Engineering and IT', 'ENG001', 5, '12345678900', 'eng.it@example.com');

INSERT INTO faculty (id, name, code, no_of_department, phone_number, email)
VALUES (102, 'Faculty of Sciences', 'SCI002', 4, '98765432150', 'sciences@example.com');

INSERT INTO faculty (id, name, code, no_of_department, phone_number, email)
VALUES (103, 'Faculty of Humanities', 'HUM003', 3, '55555555555', 'humanities@example.com');

INSERT INTO faculty (id, name, code, no_of_department, phone_number, email)
VALUES (104, 'Faculty of Business and Economics', 'BUS004', 6, '99999999999', 'business.economics@example.com');

INSERT INTO faculty (id, name, code, no_of_department, phone_number, email)
VALUES (105, 'Faculty of Social Sciences', 'SOC005', 2, '11111111111', 'social.sciences@example.com');





INSERT INTO student (id, first_name, last_name, email, registered_semester, tuition_fee)
VALUES (101, 'John', 'Smith', 'john.smith@example.com', 8, 5000.00);

INSERT INTO student (id, first_name, last_name, email, registered_semester, tuition_fee)
VALUES (102, 'Johnson', 'Michael', 'johnson.michael@example.com', 5, 4500.00);

INSERT INTO student (id, first_name, last_name, email, registered_semester, tuition_fee)
VALUES (103, 'Smith', 'Davis', '.smith@example.com', 10, 6000.00);

INSERT INTO student (id, first_name, last_name, email, registered_semester, tuition_fee)
VALUES (104, 'Emily', 'Davis', 'emily.davis@example.com', 3, 4000.00);

INSERT INTO student (id, first_name, last_name, email, registered_semester, tuition_fee)
VALUES (105, 'David', 'Wilson', 'david.wilson@example.com', 7, 5500.00);




INSERT INTO department (id, name, head_of_department, email, phone_number)
VALUES (101, 'Department of Computer Science', 'Dr. Robert Johnson', 'csdept@example.com', '12345678902');

INSERT INTO department (id, name, head_of_department, email, phone_number)
VALUES (102, 'Department of Electrical Engineering', 'Prof. Sarah Davis', 'eeedept@example.com', '98765432101');

INSERT INTO department (id, name, head_of_department, email, phone_number)
VALUES (103, 'Department of Mathematics', 'Dr. Michael Smith', 'mathdept@example.com', '55555555555');

INSERT INTO department (id, name, head_of_department, email, phone_number)
VALUES (104, 'Department of English Literature', 'Prof. Jennifer Thompson', 'englitdept@example.com', '99999999999');

INSERT INTO department (id, name, head_of_department, email, phone_number)
VALUES (105, 'Department of Physics', 'Dr. David Wilson', 'physicsdept@example.com', '11111111111');


INSERT INTO subject (id, name, credit_hours, code, description)
VALUES (101, 'Introduction to Computer Science', 3, 'CSE101', 'An introductory course covering the fundamentals of computer science.');

INSERT INTO subject (id, name, credit_hours, code, description)
VALUES (102, 'Calculus I', 4, 'MATH101', 'A foundational course in calculus covering topics such as limits, derivatives, and integrals.');

INSERT INTO subject (id, name, credit_hours, code, description)
VALUES (103, 'English Composition', 3, 'ENG102', 'A course focused on developing effective writing skills and critical thinking.');

INSERT INTO subject (id, name, credit_hours, code, description)
VALUES (104, 'Introduction to Psychology', 3, 'PSY101', 'An introductory course exploring the basic concepts and principles of psychology.');

INSERT INTO subject (id, name, credit_hours, code, description)
VALUES (105, 'Physics I', 4, 'PHYS201', 'A course covering classical mechanics, motion, forces, and energy.');



INSERT INTO address (id, street, city, state, zip_code)
VALUES (101, '123 Main Street', 'New York', 'New York', '10001');

INSERT INTO address (id, street, city, state, zip_code)
VALUES (102, '456 Elm Avenue', 'Los Angeles', 'California', '90001');

INSERT INTO address (id, street, city, state, zip_code)
VALUES (103, '789 Oak Drive', 'Chicago', 'Illinois', '60601');

INSERT INTO address (id, street, city, state, zip_code)
VALUES (104, '321 Pine Lane', 'Houston', 'Texas', '77001');

INSERT INTO address (id, street, city, state, zip_code)
VALUES (105, '987 Cedar Road', 'Miami', 'Florida', '33101');