package com.StudentManagementSystem.model;

import jakarta.validation.constraints.Email;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "faculty")
@SequenceGenerator(name = "generator", sequenceName = "faculty_seq", allocationSize = 1)
public class Faculty extends AbstractEntity<Long> {
    @NotNull(message = "Name cannot be empty")
    @Size(min = 2, max = 50, message = "Name length must be between 2 and 50")
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull(message = "Code cannot be empty")
    @Size(min = 6, max = 6, message = "Code length must be 9")
    @Column(name = "code", nullable = false)
    private String code;

    @NotNull(message = "NoOfDepartments cannot be empty")
    @Column(name = "no_of_department")
    private int numberOfDepartments;

    @NotNull(message = "Phone number cannot be empty")
    @Size(min = 11, max = 11, message = "Phone number length must be 11")
    @Column(name = "phone_number", nullable = false)
    private String phoneNumber;

    @NotNull(message = "Email cannot be empty")
    @Email(message = "Invalid Email Address")
    @Column(name = "email", nullable = false)
    private String email;

    @OneToMany(mappedBy = "faculty", cascade = CascadeType.ALL)
    private List<Subject> subjects;

}

