package com.StudentManagementSystem.model;

import jakarta.validation.constraints.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "student")
@SequenceGenerator(name = "generator", sequenceName = "student_seq", allocationSize = 1)
public class Student extends AbstractEntity<Long> {

    @NotNull(message = "First name cannot be empty")
    @Size(min = 2, max = 50, message = "First name length must be between 2 and 50")
    @Column(name = "first_name", nullable = false)
    @NotBlank
    private String firstName;

    @NotNull(message = "Last name cannot be empty")
    @Size(min = 2, max = 50, message = "Last name length must be between 2 and 50")
    @Column(name = "last_name", nullable = false)
    @NotBlank
    private String lastName;

    @Email(message = "Invalid Email Address")
    @NotNull(message = "Email cannot be empty")
    @Column(name = "email", nullable = false)
    @NotBlank
    private String email;

    @NotNull(message = "Semesters Registered can not be empty")
    @Column(name = "registered_semester", nullable = false)
    @Min(value = 1, message = "Semesters Registered must be at least 1")
    @Max(value = 14, message = "Semesters Registered cannot exceed 14")
    private int semestersRegistered;

    @Column(name = "tuition_fee", nullable = false)
    @NotNull(message = "Tuition Fee can not be empty")
    private double tuitionFeePerSemester;

    @ManyToMany
    @JoinTable(
            name = "student_subject",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id")
    )
    private List<Subject> subjects;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private Department department;

}
