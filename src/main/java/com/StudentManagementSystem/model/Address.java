package com.StudentManagementSystem.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "address")
@SequenceGenerator(name = "generator", sequenceName = "address_seq", allocationSize = 1)
public class Address extends AbstractEntity<Long> {
    @NotNull(message = "Street cannot be empty")
    @Size(min = 2, max = 100, message = "Street length must be between 2 and 100")
    @Column(name = "street", nullable = false)
    private String street;

    @NotNull(message = "City cannot be empty")
    @Size(min = 2, max = 50, message = "City length must be between 2 and 50")
    @Column(name = "city", nullable = false)
    private String city;

    @NotNull(message = "State cannot be empty")
    @Size(min = 2, max = 50, message = "State length must be between 2 and 50")
    @Column(name = "state", nullable = false)
    private String state;

    @NotNull(message = "Zip code cannot be empty")
    @Size(min = 5, max = 10, message = "Zip code length must be between 5 and 10")
    @Column(name = "zip_code", nullable = false)
    private String zipCode;

    @OneToOne(mappedBy = "address")
    private Student student;
}
