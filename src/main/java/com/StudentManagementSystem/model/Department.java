package com.StudentManagementSystem.model;

import jakarta.validation.constraints.Email;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "department")
@SequenceGenerator(name = "generator", sequenceName = "department_seq", allocationSize = 1)
public class Department extends AbstractEntity<Long> {

    @NotNull(message = "Name cannot be empty")
    @Size(min = 2, max = 50, message = "Name length must be between 2 and 50")
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull(message = "Head of Department cannot be empty")
    @Size(min = 2, max = 50, message = "Head of Department length must be between 2 and 50")
    @Column(name = "head_of_department", nullable = false)
    private String headOfDepartment;

    @Email(message = "Invalid Email Address")
    @NotNull(message = "Email cannot be empty")
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull(message = "Phone number cannot be empty")
    @Size(min = 11, max = 11, message = "Phone number length must be between 10 and 20")
    @Column(name = "phone_number", nullable = false)
    private String phoneNumber;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private Faculty faculty;

}
