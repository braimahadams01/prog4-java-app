package com.StudentManagementSystem.model;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "subject")
@SequenceGenerator(name = "generator", sequenceName = "subject_seq", allocationSize = 1)
public class Subject extends AbstractEntity<Long> {
    @NotNull(message = "Name cannot be empty")
    @Size(min = 2, max = 50, message = "Name length must be between 2 and 50")
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull(message = "Credit hours cannot be empty")
    @Column(name = "credit_hours", nullable = false)
    @Min(value = 1, message = "Minimum credit hours should be 1")
    @Max(value = 10, message = "Credit hours should not be greater than 10")
    private int creditHours;

    @NotNull(message = "Code cannot be empty")
    @Size(min = 6, max = 9, message = "Code length must be between 2 and 10")
    @Column(name = "code", nullable = false)
    private String code;

    @Size(min = 2, max = 100, message = "Description length must be between 2 and 50")
    @Column(name = "description")
    private String description;

    @ManyToMany(mappedBy = "subjects")
    private List<Student> students;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private Faculty faculty;
}
