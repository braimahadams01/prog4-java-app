package com.StudentManagementSystem.service;

import com.StudentManagementSystem.exceptions.NotFoundException;
import com.StudentManagementSystem.model.Address;
import com.StudentManagementSystem.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AddressService {

    private final AddressRepository addressRepository;

    @Autowired
    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public List<Address> getAllAddresses() {
        return addressRepository.findAll();
    }

    public Optional<Address> getAddressById(Long id) {
        return addressRepository.findById(id);
    }

    @Transactional
    public Address saveAddress(Address address) {
        return addressRepository.save(address);
    }

    @Transactional
    public void deleteAddressById(Long id) throws NotFoundException {
        Address address = addressRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Address not found with id: %d", id)));

        addressRepository.delete(address);
    }
}
