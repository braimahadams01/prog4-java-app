package com.StudentManagementSystem.service;

import com.StudentManagementSystem.exceptions.NotFoundException;
import com.StudentManagementSystem.model.Department;
import com.StudentManagementSystem.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {

    private final DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentService(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    public List<Department> getAllDepartments() {
        return departmentRepository.findAll();
    }

    public Optional<Department> getDepartmentById(Long id) {
        return departmentRepository.findById(id);
    }

    @Transactional
    public Department saveDepartment(Department department) {
        return departmentRepository.save(department);
    }

    @Transactional
    public void deleteDepartmentById(Long id) throws NotFoundException {
        Department department = departmentRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Department not found with id: %d", id)));

        departmentRepository.delete(department);
    }
}
