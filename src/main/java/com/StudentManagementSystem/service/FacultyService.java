package com.StudentManagementSystem.service;

import com.StudentManagementSystem.exceptions.NotFoundException;
import com.StudentManagementSystem.model.Faculty;
import com.StudentManagementSystem.repository.FacultyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class FacultyService {

    private final FacultyRepository facultyRepository;

    @Autowired
    public FacultyService(FacultyRepository facultyRepository) {
        this.facultyRepository = facultyRepository;
    }

    public List<Faculty> getAllFaculties() {
        return facultyRepository.findAll();
    }

    public Optional<Faculty> getFacultyById(Long id) {
        return facultyRepository.findById(id);
    }

    @Transactional
    public Faculty saveFaculty(Faculty faculty) {
        return facultyRepository.save(faculty);
    }

    @Transactional
    public void deleteFacultyById(Long id) throws NotFoundException {
        Faculty faculty = facultyRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Faculty not found with id: %d", id)));

        facultyRepository.delete(faculty);
    }
}
