package com.StudentManagementSystem.service;

import com.StudentManagementSystem.exceptions.NotFoundException;
import com.StudentManagementSystem.model.Subject;
import com.StudentManagementSystem.repository.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SubjectService {

    private final SubjectRepository subjectRepository;

    @Autowired
    public SubjectService(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    public List<Subject> getAllSubjects() {
        return subjectRepository.findAll();
    }

    public Optional<Subject> getSubjectById(Long id) {
        return subjectRepository.findById(id);
    }

    @Transactional
    public Subject saveSubject(Subject subject) {
        return subjectRepository.save(subject);
    }

    @Transactional
    public void deleteSubjectById(Long id) throws NotFoundException {
        Subject subject = subjectRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Subject not found with id: %d", id)));

        subjectRepository.delete(subject);
    }
}
