package com.StudentManagementSystem.controller.web;

import com.StudentManagementSystem.exceptions.NotFoundException;
import com.StudentManagementSystem.model.Student;
import com.StudentManagementSystem.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/students")
public class StudentWebController {

    private final StudentService studentService;

    @Autowired
    public StudentWebController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public String getAllStudents(Model model) {
        List<Student> students = studentService.getAllStudents();
        model.addAttribute("students", students);
        return "student-list";
    }

    @GetMapping("/search")
    public String findStudentById(@RequestParam("studentId") Long id, Model model) throws NotFoundException {
        Student student = studentService.getStudentById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Student not found with id: %d", id)));
        model.addAttribute("student", student);
        return "student-details";
    }

    @GetMapping("/create")
    public String showCreateForm(Model model) {
        model.addAttribute("student", new Student());
        return "student-form";
    }

    @PostMapping("/create")
    public String createStudent(@Valid @ModelAttribute("student") Student student, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "student-form";
        }
        studentService.saveStudent(student);
        return "redirect:/students";
    }

    @GetMapping("/{id}/edit")
    public String showEditForm(@PathVariable Long id, Model model) throws NotFoundException {
        Student student = studentService.getStudentById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Student not found with id: %d", id)));
        model.addAttribute("student", student);
        return "student-edit";
    }

    @PostMapping("/{id}/edit")
    public String updateStudent(@PathVariable Long id, @Valid @ModelAttribute("student") Student student,
                                BindingResult bindingResult) throws NotFoundException {
        if (bindingResult.hasErrors()) {
            return "student-edit";
        }
        Student existingStudent = studentService.getStudentById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Student not found with id: %d", id)));
        student.setId(existingStudent.getId()); // Preserve the existing ID
        studentService.saveStudent(student);
        return "redirect:/students";
    }

    @GetMapping("/{id}/delete")
    public String deleteStudent(@PathVariable Long id) throws NotFoundException {
        studentService.deleteStudentById(id);
        return "redirect:/students";
    }
}
