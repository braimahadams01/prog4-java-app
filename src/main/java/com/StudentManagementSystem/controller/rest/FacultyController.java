package com.StudentManagementSystem.controller.rest;

import com.StudentManagementSystem.exceptions.NotFoundException;
import com.StudentManagementSystem.model.Faculty;
import com.StudentManagementSystem.service.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/faculties")
public class FacultyController {

    private final FacultyService facultyService;

    @Autowired
    public FacultyController(FacultyService facultyService) {
        this.facultyService = facultyService;
    }

    @GetMapping
    public ResponseEntity<List<Faculty>> getAllFaculties() {
        List<Faculty> faculties = facultyService.getAllFaculties();
        return ResponseEntity.ok(faculties);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Faculty> getFacultyById(@PathVariable Long id) throws NotFoundException {
        return this.facultyService.getFacultyById(id)
                .map(faculty -> new ResponseEntity<>(faculty, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

    }

    @PostMapping
    public ResponseEntity<Faculty> createFaculty(@RequestBody Faculty faculty) {
        Faculty createdFaculty = facultyService.saveFaculty(faculty);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdFaculty);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Faculty> updateFaculty(@PathVariable Long id, @RequestBody Faculty faculty)
            throws NotFoundException {
        Faculty existingFaculty = facultyService.getFacultyById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Faculty not found with id: %d", id)));
        faculty.setId(existingFaculty.getId()); // Preserve the existing ID
        Faculty updatedFaculty = facultyService.saveFaculty(faculty);
        return ResponseEntity.ok(updatedFaculty);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteFaculty(@PathVariable Long id) throws NotFoundException {
        facultyService.deleteFacultyById(id);
        return ResponseEntity.noContent().build();
    }
}
