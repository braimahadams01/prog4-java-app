package com.StudentManagementSystem.controller.rest;

import com.StudentManagementSystem.exceptions.NotFoundException;
import com.StudentManagementSystem.model.Subject;
import com.StudentManagementSystem.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/subjects")
public class SubjectController {

    private final SubjectService subjectService;

    @Autowired
    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @GetMapping
    public ResponseEntity<List<Subject>> getAllSubjects() {
        List<Subject> subjects = subjectService.getAllSubjects();
        return ResponseEntity.ok(subjects);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Subject> getSubjectById(@PathVariable Long id) throws NotFoundException {
        return this.subjectService.getSubjectById(id)
                .map(subject -> new ResponseEntity<>(subject, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<Subject> createSubject(@RequestBody Subject subject) {
        Subject createdSubject = subjectService.saveSubject(subject);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdSubject);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Subject> updateSubject(@PathVariable Long id, @RequestBody Subject subject)
            throws NotFoundException {
        Subject existingSubject = subjectService.getSubjectById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Subject not found with id: %d", id)));
        subject.setId(existingSubject.getId()); // Preserve the existing ID
        Subject updatedSubject = subjectService.saveSubject(subject);
        return ResponseEntity.ok(updatedSubject);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSubject(@PathVariable Long id) throws NotFoundException {
        subjectService.deleteSubjectById(id);
        return ResponseEntity.noContent().build();
    }
}
