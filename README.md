# Student Management System

This project is a Student Management System web application built using Java with JPA for database operations and Spring Boot for the web framework.

## Overview

The Student Management System is designed to manage student, faculty, department, subject, and address information. It provides CRUD (Create, Read, Update, Delete) operations via both a web interface and REST endpoints.

![Student Management System](./Screenshot%202024-06-03.png)

## Accessing the application

- Build and run the project using: "mvn spring-boot:run"

- and Access the web interface at [http://localhost:8080]

## Database Initialization

The application uses HSQLDB database with the following tables:

- Address
- Department
- Faculty
- Student
- Subject

## Project Structure

The project structure includes Java packages for controllers, models, repositories, security configurations, services, and the main application class. HTML templates for the web interface are stored in the `src/main/resources/templates` directory.
